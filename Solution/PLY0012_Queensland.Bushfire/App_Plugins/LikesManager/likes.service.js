﻿angular.module("umbraco.services").factory("likesService", function ($http) {
    return {
        delete: function (id) {
            return $http.delete("/umbraco/backoffice/API/LikesManager/DeleteById/" + id);
        },
        getAll: function (id) {
            return $http.get("/umbraco/backoffice/API/LikesManager/Getall/" + id);
        }
    }
});