﻿angular.module("umbraco").controller("Level2.LikesManagerController",

    function ($scope, $http, entityResource, editorState, likesService) {
        
        var currentNodeId = editorState.current.id;
        console.log("Current node Id " + currentNodeId);

        $scope.load = function () {

            // perform loading here..
            //$http.get("/umbraco/backoffice/API/LikesManager/Getall/" + currentNodeId)
            // use likesService
            likesService.getAll(currentNodeId)
                .then(function (response) {
                    var likes = response.data;
                    // for each like found, we want to retrieve the associated member
                    angular.forEach(likes, function (item) {
                        entityResource.getById(item.ChildId, "Member").then(function (ent) {
                            // append the entity to the likes object
                            item.member = ent;
                        });
                    });

                    $scope.likes = likes;
                    $scope.model.value = likes.length;
                });

        };

        $scope.delete = function(like) {
            //$http.delete("/umbraco/backoffice/API/LikesManager/DeleteById/" + like.Id)
            likesService.delete(like.Id)
                .then(function () {
                    // reload everything to ensure we get up-to-date data
                    $scope.load();
                });
        };

        $scope.load();
    }
);