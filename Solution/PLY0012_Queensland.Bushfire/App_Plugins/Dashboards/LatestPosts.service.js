﻿    angular.module("umbraco.services").factory("latestsPostsService", function ($http) {  
        return {
            getAll : function(){
                return $http.get("/umbraco/backoffice/api/LatestPostsDashboard/GetLatestsPost");
            },
            publish: function (id) {
                return $http.get("/umbraco/backoffice/api/LatestPostsDashboard/PublishPost/" + id);
            },
            unpublish: function (id) {
                return $http.get("/umbraco/backoffice/api/LatestPostsDashboard/UnpublishPost/" + id);
            }
        };
    });