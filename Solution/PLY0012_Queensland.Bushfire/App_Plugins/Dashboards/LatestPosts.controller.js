﻿angular.module("umbraco")
    .controller("level2.latestsPostsController", 
    function ($scope, $http, latestsPostsService, entityResource) {
        $scope.load = function () {
            latestsPostsService.getAll().success(function (data) {
                $scope.posts = data;
            });
        };

        $scope.publish = function(post) {
            latestsPostsService.publish(post.Id)
                .then(function () {
                    // reload everything to ensure we get up-to-date data
                    $scope.load();
                });
        };

        $scope.unpublish = function (post) {
            latestsPostsService.unpublish(post.Id)
                .then(function () {
                    // reload everything to ensure we get up-to-date data
                    $scope.load();
                });
        };

        $scope.load();
    });
