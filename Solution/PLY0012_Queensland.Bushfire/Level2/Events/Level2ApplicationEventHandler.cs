﻿using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;

namespace PLY0012_Queensland.Bushfire.Level2.Events
{
    public class Level2ApplicationEventHandler : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication,ApplicationContext applicationContext)
        {
            ContentService.Publishing += ContentService_Publishing;
        }

        void ContentService_Publishing(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            var badWords = new[] { "nike", "adidas", "converse", "vans", "cheap"};
            foreach (var content in e.PublishedEntities) {
                if (content.ContentType.Alias == "statusUpdate") {
                    var bodyText = content.GetValue<string>("bodyText").ToLower();
                    if (badWords.Any(bodyText.Contains))
                    {
                        e.Cancel = true;
                        return;
                    }
                }
            }
        }

    }
}