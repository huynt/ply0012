using Castle.Windsor;
using Glass.Mapper.Configuration;
using Glass.Mapper.Umb.CastleWindsor;
using Glass.Mapper.Umb.Configuration.Attributes;

namespace PLY0012_Queensland.Bushfire
{
    public static class GlassMapperUmbCustom
    {
		public static void CastleConfig(IWindsorContainer container){
			var config = new Config();
			container.Install(new UmbracoInstaller(config));
		}

		public static IConfigurationLoader[] GlassLoaders(){
			var attributes = new UmbracoAttributeConfigurationLoader("REC0314_Durex30NON");
			
			return new IConfigurationLoader[]{attributes};
		}
    }
}
