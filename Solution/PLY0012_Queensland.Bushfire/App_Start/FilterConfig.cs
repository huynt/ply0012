﻿using System.Web;
using System.Web.Mvc;

namespace PLY0012_Queensland.Bushfire
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}