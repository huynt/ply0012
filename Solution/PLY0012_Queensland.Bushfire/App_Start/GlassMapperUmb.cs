using Glass.Mapper.Umb.CastleWindsor;
using PLY0012_Queensland.Bushfire;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(GlassMapperUmb), "Start")]

namespace PLY0012_Queensland.Bushfire
{
    public static class  GlassMapperUmb
    {
        public static void Start()
        {
            //create the resolver
            var resolver = DependencyResolver.CreateStandardResolver();

			//install the custom services
			GlassMapperUmbCustom.CastleConfig(resolver.Container);

            //create a context
            var context = Glass.Mapper.Context.Create(resolver);
            context.Load(    
				GlassMapperUmbCustom.GlassLoaders()   
                );
        }
    }
}