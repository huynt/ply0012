﻿using System;

namespace PLY0012_Queensland.Bushfire.Models
{
    public class LikeModel
    {
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }

        public string MemberName { get; set; }
    }
}