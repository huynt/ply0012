﻿using System.Collections.Generic;
using Umbraco.Core.Models;

namespace PLY0012_Queensland.Bushfire.Models
{
    public class SearchModel
    {
        public string Query { get; set; }
	    public IEnumerable<IPublishedContent> SearchResults { get; set; }
    }
}