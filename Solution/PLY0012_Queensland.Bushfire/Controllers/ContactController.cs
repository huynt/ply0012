﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using PLY0012_Queensland.Bushfire.Models;
using Umbraco.Web.Mvc;
using System.CodeDom;

namespace PLY0012_Queensland.Bushfire.Controllers
{
    public class ContactController : SurfaceController
    {
        [HttpPost]
        public ActionResult Submit(ContactModel model)
        {
            int a;

            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            // do smth
            // send email
            var message = new MailMessage();
            message.To.Add("siteadmin@gmail.com");
            message.Subject = "New Contact request";
            message.From = new MailAddress(model.Email, model.Name);
            message.Body = model.Message;
            var smtp = new SmtpClient();
            //smtp.PickupDirectoryLocation = "C:\\inetpub\\smtp\\pickup\\";
            //smtp.Send(message);
            TempData["success"] = true;

            return RedirectToCurrentUmbracoPage();
        }

    }
}
