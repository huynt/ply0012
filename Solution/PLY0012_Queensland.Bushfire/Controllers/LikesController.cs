﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.WebApi;

namespace PLY0012_Queensland.Bushfire.Controllers
{
    [MemberAuthorize(AllowType="IntranetUser")]
    public class LikesController : UmbracoApiController
    {
        [System.Web.Http.HttpGet]
        public int LikeStatus(int id) {
            var contentService = Services.ContentService;

            var memberService = Services.MemberService;
            var relationService = Services.RelationService;

            var member = memberService.GetById(Members.GetCurrentMemberId());

            var post = contentService.GetById(id);

            if (!relationService.AreRelated(post, member, "likes")) {
                relationService.Relate(post, member, "likes");
            }

            var likes = relationService.GetByParentOrChildId(id)
                        .Count(x => x.RelationType.Alias == "likes");

            int numberOfLikes;
            var currentLikes = post.GetValue("likes").ToString();
            int.TryParse(currentLikes, out numberOfLikes);
            numberOfLikes = numberOfLikes + 1;

            post.SetValue("likes", likes);
            contentService.PublishWithStatus(post);

            return likes;
        }
    }
}
