﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.WebApi;

namespace PLY0012_Queensland.Bushfire.Controllers
{
    public class LikesManagerController : UmbracoAuthorizedApiController
    {
        public IEnumerable<Umbraco.Core.Models.IRelation> Getall(int id) {
            var relations = Services.RelationService.GetByParentId(id);
            return relations;
        }

        public HttpResponseMessage DeleteById(int id) {
            var relation = Services.RelationService.GetById(id);
            if (relation == null)
                return new HttpResponseMessage(System.Net.HttpStatusCode.NotFound);

            int parentId = relation.ParentId;

            Services.RelationService.Delete(relation);
            var cs = Services.ContentService;
            cs.SaveAndPublishWithStatus(cs.GetById(parentId));
            return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
        }
    }
}
