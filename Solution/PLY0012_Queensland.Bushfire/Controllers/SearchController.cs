﻿using Examine;
using Examine.SearchCriteria;
using System.Linq;
using System.Web.Mvc;
using PLY0012_Queensland.Bushfire.Models;
using Umbraco.Web.Mvc;

namespace PLY0012_Queensland.Bushfire.Controllers
{
    public class SearchController : SurfaceController
    {
        [ChildActionOnly]
        public ActionResult Search(string query)
        {
            var model = new SearchModel { Query = query };
            if (!string.IsNullOrEmpty(query)) {
                // search criteria #1
                //var criteria = ExamineManager.Instance.CreateSearchCriteria(BooleanOperation.Or)
                //    .NodeTypeAlias("statusUpdate").And().Field("bodyText", query).Compile();

                // boost
                //var criteria = ExamineManager.Instance.CreateSearchCriteria(BooleanOperation.Or)
                //    .RawQuery("nodeName: " + query + "^2 bodyText:" + query);

                // fuzzy
                var criteria = ExamineManager.Instance.CreateSearchCriteria(BooleanOperation.Or)
                    .RawQuery("nodeName: " + query + "~0.7 bodyText:" + query);

                model.SearchResults = Umbraco.TypedSearch(criteria);
            }
            return PartialView("~/Views/Partials/SearchPartial.cshtml", model);
        }
    }
}
