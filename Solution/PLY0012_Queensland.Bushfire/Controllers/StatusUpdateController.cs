﻿using System.Web.Mvc;
using PLY0012_Queensland.Bushfire.Models;
using Umbraco.Web.Mvc;
using PLY0012_Queensland.Bushfire.Level2;
using System.Collections.Generic;

namespace PLY0012_Queensland.Bushfire.Controllers
{
    public class StatusUpdateController : SurfaceController
    {
        public PartialViewResult Render() {
            return PartialView("StatusUpdatePartial", new StatusUpdateModel());
        }        
        public ActionResult Create(StatusUpdateModel model)
        {
            //var fakeValue = "This is a test";
            //fakeValue = fakeValue.Substring(999);

            //if (!ModelState.IsValid)
            //    return CurrentUmbracoPage();

            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            var currentPageId = UmbracoContext.PageId.Value;
            var contentService = Services.ContentService;
            var statusUpdateTitle = Umbraco.Truncate(Umbraco.StripHtml(model.BodyText), 50).ToString();
            var content = contentService.CreateContent(statusUpdateTitle, currentPageId, "statusUpdate");
            content.SetValue("bodyText", model.BodyText);

            var ms = Services.MediaService;
            if (model.Files.HasFiles() && model.Files.ContainsImages())
            {
                var folder = ms.CreateMedia(statusUpdateTitle, -1, "Folder");
                ms.Save(folder);
                content.SetValue("mediaFolder", folder.Id);

                List<string> relatedMedia = new List<string>();

                foreach (var file in model.Files) {
                    if (file.IsImage()) {
                        var image = ms.CreateMedia(file.FileName, folder, "Image");
                        image.SetValue("umbracoFile", file);
                        ms.Save(image);
                        relatedMedia.Add(image.Id.ToString());
                    }
                }

                content.SetValue("relatedMedia", string.Join(",", relatedMedia.ToArray()));
            }

            // member
            if (Members.IsLoggedIn()) {
                var memberId = Members.GetCurrentMemberId();
                content.SetValue("memberId", memberId);
            }

            contentService.PublishWithStatus(content);

            return RedirectToCurrentUmbracoPage();
        }
    }
}