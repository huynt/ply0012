﻿using System.Web.Mvc;
using PLY0012_Queensland.Bushfire.Models;
using Umbraco.Web.Mvc;

namespace PLY0012_Queensland.Bushfire.Controllers
{
    public class RegisterController : SurfaceController
    {
        public ActionResult Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            var memberService = Services.MemberService;
            if (memberService.GetByEmail(model.Email) != null) {
                ModelState.AddModelError("", "A member with that email already exists");
                return CurrentUmbracoPage();
            }

            var member = memberService.CreateMemberWithIdentity(model.Email, model.Email, model.Name, "IntranetUser");
            member.SetValue("biography", model.Biography);
            member.SetValue("avatar", model.Avatar);

            memberService.Save(member);
            memberService.AssignRole(member.Id, "Intranet Users");

            memberService.SavePassword(member, model.Password);

            // log user in
            Members.Login(model.Email, model.Password);

            return Redirect("/");
        }
    }
}
