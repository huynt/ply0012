﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Umbraco.Web.WebApi;

namespace PLY0012_Queensland.Bushfire.Controllers
{
    public class LatestPostsDashboardController : UmbracoAuthorizedApiController
    {
        public IEnumerable<StatusUpdateViewModel> GetLatestsPost()
        {
            // get the type by type alias
            var type = Services.ContentTypeService.GetContentType("statusUpdate");

            // get latest nodes
            var content = from item in
                              Services.ContentService.GetContentOfContentType(type.Id).OrderByDescending(x => x.CreateDate).Take(10)
                          select new StatusUpdateViewModel { 
                            Id = item.Id,
                            Text = item.GetValue<string>("bodyText"),
                            CreateDate = item.CreateDate.ToString(),
                            MemberName = item.GetValue<int>("memberId") > 0 ? Services.MemberService.GetById(item.GetValue<int>("memberId")).Name : "Guests",
                            PublishStatus = item.Published ? "published" : "unpublished"
                          };
            return content;
        }

        public bool PublishPost(int id) {
            var cs = Services.ContentService;
            var item = cs.GetById(id);
            if (item == null)
                return false;

            cs.SaveAndPublishWithStatus(item);

            return true;
        }

        public bool UnpublishPost(int id)
        {
            var cs = Services.ContentService;
            var item = cs.GetById(id);
            if (item == null)
                return false;

            cs.UnPublish(item);

            return true;
        }
    }

    public class StatusUpdateViewModel
    {
        [JsonProperty("text")]
        public string Text { get; set; }
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("memberName")]
        public string MemberName { get; set; }
        [JsonProperty("createDate")]
        public string CreateDate { get; set; }
        [JsonProperty("publishStatus")]
        public string PublishStatus { get; set; }
    }
}